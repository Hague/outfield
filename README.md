# Outfield

Simple OBS remote control, supports

* image of current scene,
* scene switching, and
* starting/stopping recording/streaming.

Requires the [OBS Websocket][obswebsocket] plugin on OBS.

Uses a modified version of the [websocket-obs-java][wsjava] library to support
recording and preview images.

Feature requests considered.

Available on [F-Droid](https://f-droid.org/packages/com.matt.outfield/).

## Screenshot

![Light Screenshot of Outfield](readme-img/light.png)
![Dark Screenshot of Outfield](readme-img/dark.png)

[obswebsocket]: https://obsproject.com/forum/resources/obs-websocket-remote-control-obs-studio-from-websockets.466/
[wsjava]: https://github.com/Twasi/websocket-obs-java
