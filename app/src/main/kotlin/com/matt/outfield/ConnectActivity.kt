package com.matt.outfield

import android.content.Intent
import android.os.Bundle
import android.view.View;
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager

public class ConnectActivity : AppCompatActivity() {
    val PROTOCOL = "ws://"
    val DEFAULT_PORT = "4444"

    val PREF_HOSTNAME = "hostname"
    val PREF_PORT = "port"
    val PREF_PASSWORD = "password"
    val PREF_SAVE_PASSWORD = "save_password"

    var hostname : EditText? = null
    var port : EditText? = null
    var password : EditText? = null
    var savePassword : CheckBox? = null

    override protected fun onCreate(savedInstanceState : Bundle?) {
        enableEdgeToEdge()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.connect)
        initialiseWidgets()
    }

    private fun initialiseWidgets() {
        hostname = findViewById(R.id.hostname)
        port = findViewById(R.id.port)
        password = findViewById(R.id.password)
        savePassword = findViewById(R.id.save_password)

        readPrefs()

        val connect : Button? = findViewById(R.id.connect)
        connect?.setOnClickListener(object : View.OnClickListener {
            override public fun onClick(view : View) {
                launchRemote()
            }
        })
    }

    private fun launchRemote() {
        writePrefs()

        val port = port?.getText().toString()
        val pass = password?.getText().toString()

        val intent = Intent(this, RemoteActivity::class.java)
        intent.putExtra(ARG_HOSTNAME, getHostname())
        intent.putExtra(ARG_PORT, port)
        intent.putExtra(ARG_PASSWORD, pass)

        this.startActivity(intent);
    }

    private fun getHostname() : String {
        var host = hostname?.getText().toString()
        if (host.startsWith(PROTOCOL)) {
            host = host.substring(PROTOCOL.length, host.length)
        }
        return host
    }

    private fun readPrefs() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this);
        hostname?.setText(prefs.getString(PREF_HOSTNAME, PROTOCOL))
        port?.setText(prefs.getString(PREF_PORT, DEFAULT_PORT))
        password?.setText(prefs.getString(PREF_PASSWORD, ""))
        savePassword?.setChecked(prefs.getBoolean(PREF_SAVE_PASSWORD, false))
    }

    private fun writePrefs() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this);
        val editor = prefs.edit()

        editor.putString(PREF_HOSTNAME,
                         hostname?.getText().toString())
        editor.putString(PREF_PORT,
                         port?.getText().toString())

        val save = savePassword?.isChecked() ?: false

        editor.putBoolean(PREF_SAVE_PASSWORD, save)

        if (save)
            editor.putString(PREF_PASSWORD, password?.getText().toString())
        else
            editor.putString(PREF_PASSWORD, "")

        editor.apply();
    }
}
