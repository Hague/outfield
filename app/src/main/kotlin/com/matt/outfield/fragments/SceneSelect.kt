
package com.matt.outfield.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckedTextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

import com.matt.outfield.R
import com.matt.outfield.RemoteViewModel

public class SceneSelect() : Fragment() {

    private val viewModel : RemoteViewModel by activityViewModels()

    private lateinit var sceneListView : RecyclerView
    private lateinit var adapter : SceneSelectAdapter

    override fun onCreateView(inflater : LayoutInflater,
                              container : ViewGroup?,
                              savedInstanceState : Bundle?) : View {

        val view = inflater.inflate(R.layout.scene_select,
                                    container,
                                    false)

        sceneListView = view.findViewById(R.id.scene_list)

        val layoutManager = LinearLayoutManager(context);
        sceneListView.setLayoutManager(layoutManager);
        sceneListView.setItemAnimator(DefaultItemAnimator());
        sceneListView.addItemDecoration(
            DividerItemDecoration(context,
                                  DividerItemDecoration.VERTICAL)
        );

        adapter = SceneSelectAdapter()
        sceneListView.setAdapter(adapter)

        viewModel.sceneList.observe(
            viewLifecycleOwner,
            Observer<List<String>> { adapter.submitList(it) }
        )

        viewModel.currentScene.observe(
            viewLifecycleOwner,
            Observer<String?> { adapter.currentScene = it }
        )

        return view
    }

    inner private class SceneHolder(val view : CheckedTextView)
            : RecyclerView.ViewHolder(view) {
        public var sceneName : String? = null
            set(value) {
                field = value
                view.setText(value)
            }
        public var current : Boolean = false
            set(value) {
                view.setChecked(value)
            }

        init {
            view.setOnClickListener(object : View.OnClickListener {
                override public fun onClick(view : View) {
                    val name = sceneName
                    if (name != null)
                        viewModel.switchScene(name)
                }
            });
        }
    }

    private val STRING_DIFF = object : DiffUtil.ItemCallback<String?>() {
        override fun areItemsTheSame(
            oldName : String, newName : String
        ) : Boolean = oldName == newName

        override fun areContentsTheSame(
            oldName : String, newName : String
        ) : Boolean = oldName == newName
    }

    inner private class SceneSelectAdapter()
            : ListAdapter<String?, SceneHolder>(STRING_DIFF) {

        public var currentScene : String? = null
            set(value) {
                val sceneOut = field
                field = value
                (0..getItemCount()-1).forEach {
                    val item = getItem(it)
                    if (item.equals(sceneOut) || item.equals(value))
                        notifyItemChanged(it)
                }
            }

        override public fun onCreateViewHolder(parent : ViewGroup,
                                               viewType : Int) : SceneHolder {
            val item = LayoutInflater.from(parent.getContext())
                                     .inflate(R.layout.scene_item,
                                              parent,
                                              false) as CheckedTextView;
            return SceneHolder(item);
        }

        override public fun onBindViewHolder(holder : SceneHolder,
                                             position : Int) {
            holder.sceneName = getItem(position)
            holder.current = getItem(position).equals(currentScene)
        }
    }
}
