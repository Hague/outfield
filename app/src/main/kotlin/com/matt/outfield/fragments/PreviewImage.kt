
package com.matt.outfield.fragments

import java.util.Timer
import java.util.TimerTask

import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.launch

import com.matt.outfield.R
import com.matt.outfield.RemoteViewModel

class PreviewImage() : Fragment() {

    private val viewModel : RemoteViewModel by activityViewModels()

    private lateinit var previewImageView : ImageView

    private val REFRESH_TIME = 2

    private var timer : Timer? = null

    override fun onCreateView(inflater : LayoutInflater,
                              container : ViewGroup?,
                              savedInstanceState : Bundle?) : View {

        val view = inflater.inflate(R.layout.preview_image,
                                    container,
                                    false)

        previewImageView = view.findViewById(R.id.preview_image)

        previewImageView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view : View) {
                viewModel.updateImage()
            }
        })

        viewModel.connectionStatus.observe(
            viewLifecycleOwner,
            Observer<RemoteViewModel.ConnectionStatus> { status ->
                if (status.state
                        == RemoteViewModel.ConnectionState.CONNECTED)
                    startAutomaticRefresh()
                else
                    stopAutomaticRefresh()
            }
        )

        viewModel.previewImage.observe(
            viewLifecycleOwner,
            Observer<Bitmap> { previewImageView.setImageBitmap(it) }
        )

        return view
    }

    override fun onResume() {
        super.onResume()
        if (viewModel.connectionStatus.value?.state
                == RemoteViewModel.ConnectionState.CONNECTED)
            startAutomaticRefresh()
    }

    override fun onPause() {
        super.onPause()
        stopAutomaticRefresh()
    }

    private fun startAutomaticRefresh() {
        val rate : Long = 1000L * REFRESH_TIME

        timer?.cancel()
        timer = Timer()
        timer?.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                lifecycleScope.launch {
                    viewModel.updateImage()
                }
            }
        }, 0, rate)
    }

    private fun stopAutomaticRefresh() {
        timer?.cancel()
    }
}
