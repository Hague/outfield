
package com.matt.outfield.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer

import com.matt.outfield.R
import com.matt.outfield.RemoteViewModel

public class PlayButtons() : Fragment() {

    private val viewModel : RemoteViewModel by activityViewModels()

    private lateinit var recordButton : Button
    private lateinit var streamButton : Button

    override fun onCreateView(inflater : LayoutInflater,
                              container : ViewGroup?,
                              savedInstanceState : Bundle?) : View {

        val view = inflater.inflate(R.layout.play_buttons,
                                    container,
                                    false)

        recordButton = view.findViewById(R.id.record_button) as Button
        streamButton = view.findViewById(R.id.stream_button) as Button

        recordButton.setOnClickListener(object : View.OnClickListener {
            override public fun onClick(view : View) {
                viewModel.toggleRecording()
            }
        })

        streamButton.setOnClickListener(object : View.OnClickListener {
            override public fun onClick(view : View) {
                viewModel.toggleStreaming()
            }
        })

        viewModel.isRecording.observe(
            viewLifecycleOwner,
            Observer<Boolean>() { recording ->
                if (recording)
                    recordButton.setText(R.string.stop_recording)
                else
                    recordButton.setText(R.string.start_recording)
            }
        )

        viewModel.isStreaming.observe(
            viewLifecycleOwner,
            Observer<Boolean>() { streaming ->
                if (streaming)
                    streamButton.setText(R.string.stop_streaming)
                else
                    streamButton.setText(R.string.start_streaming)
            }
        )

        return view
    }
}
