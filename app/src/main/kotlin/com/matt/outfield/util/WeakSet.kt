
package com.matt.outfield.util;

import java.util.Collections;
import java.util.WeakHashMap;

/**
 * Returns a new set of weak references
 */
fun <T> buildWeakSet() : MutableSet<T> {
    return Collections.newSetFromMap(
        Collections.synchronizedMap(
            WeakHashMap<T, Boolean>()
        )
    )
}
