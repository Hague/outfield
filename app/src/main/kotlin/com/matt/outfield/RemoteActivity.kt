
package com.matt.outfield

import android.os.Bundle
import android.view.ViewGroup.MarginLayoutParams
import android.view.WindowManager
import android.widget.TextView
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updateLayoutParams
import androidx.lifecycle.Observer

public val ARG_HOSTNAME = "hostname"
public val ARG_PORT = "port"
public val ARG_PASSWORD = "password"

public class RemoteActivity : AppCompatActivity() {

    private val viewModel : RemoteViewModel by viewModels()

    private lateinit var connectionStatus : TextView

    override protected fun onCreate(savedInstanceState : Bundle?) {
        enableEdgeToEdge()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.remote)
        connectionStatus = findViewById(R.id.connection_status) as TextView

        setupInsets()

        val intent = getIntent()
        val hostname = intent.getStringExtra(ARG_HOSTNAME)
        val port = try {
            intent.getStringExtra(ARG_PORT)?.toInt() ?: -1
        } catch (e : NumberFormatException) {
            -1
        }
        val password = intent.getStringExtra(ARG_PASSWORD)

        viewModel.hostname = hostname
        viewModel.port = port
        viewModel.password = password

        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    override protected fun onResume() {
        super.onResume()

        viewModel.connectionStatus.observe(
            this,
            Observer<RemoteViewModel.ConnectionStatus> {
                onUpdateConnectionStatus(it)
            }
        )

        viewModel.connect()
    }

    private fun setupInsets() {
        ViewCompat.setOnApplyWindowInsetsListener(connectionStatus)
        { v, windowInsets ->
            val insets = windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars()
            )
            v.updateLayoutParams<MarginLayoutParams> {
                topMargin = insets.top
                leftMargin = insets.left
                rightMargin = insets.right
            }
            WindowInsetsCompat.CONSUMED
        }
    }

    private fun
    onUpdateConnectionStatus(status : RemoteViewModel.ConnectionStatus) {

        val typeText = when (status.state) {
            RemoteViewModel.ConnectionState.CONNECTED ->
                getResources().getString(R.string.connected)
            RemoteViewModel.ConnectionState.DISCONNECTED ->
                getResources().getString(R.string.disconnected)
            RemoteViewModel.ConnectionState.FAILED ->
                getResources().getString(R.string.connection_failed)
            RemoteViewModel.ConnectionState.CONNECT_ERROR ->
                getResources().getString(R.string.connection_error)
        }
        connectionStatus.setText(
            if (status.message == null) {
                typeText
            } else {
                getResources().getString(
                    R.string.connection_status_format_with_message,
                    typeText, status.message
                )
            }
        )
    }

    override protected fun onPause() {
        super.onPause()
        viewModel.disconnect()
    }

}
