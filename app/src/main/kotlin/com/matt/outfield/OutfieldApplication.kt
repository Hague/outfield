
package com.matt.outfield

import android.app.Application
import com.google.android.material.color.DynamicColors

class OutfieldApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        DynamicColors.applyToActivitiesIfAvailable(this)
    }
}
