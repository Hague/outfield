
package com.matt.outfield

import android.app.Application
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

import com.matt.outfield.controller.OBSController

class RemoteViewModel(application : Application)
        : AndroidViewModel(application),
          OBSController.OBSListener {

    private var obsController : OBSController? = null

    var hostname : String? = null
    var port     : Int = -1
    var password : String? = null

    enum class ConnectionState {
        CONNECTED, DISCONNECTED, FAILED, CONNECT_ERROR
    }

    class ConnectionStatus(val state : ConnectionState,
                           val message : String? = null)

    val connectionStatus = MutableLiveData<ConnectionStatus>()
    val isRecording = MutableLiveData<Boolean>()
    val isStreaming = MutableLiveData<Boolean>()
    val previewImage = MutableLiveData<Bitmap>()
    val sceneList = MutableLiveData<List<String>>()
    val currentScene = MutableLiveData<String?>()

    /**
     * Connect if hostname and password is not null
     *
     * Closes existing connection if there is one
     */
    fun connect() {
        obsController?.disconnect()
        hostname?.let { hn ->
            port?.let { p ->
                password?.let { pw ->
                    obsController = OBSController(
                        OBSController.OBSConfig(hn, p, pw)
                    )
                    obsController?.addListener(this)
                    obsController?.connect()
                }
            }
        }
    }

    fun disconnect() {
        obsController?.disconnect()
    }

    fun toggleRecording() {
        if (isRecording.value ?: false)
            obsController?.stopRecording()
        else
            obsController?.startRecording()
    }

    fun toggleStreaming() {
        if (isStreaming.value ?: false)
            obsController?.stopStreaming()
        else
            obsController?.startStreaming()
    }

    fun updateImage() {
        obsController?.getCurrentScene({ sceneName ->
            sceneName.let {
                obsController?.takeSourceScreenshot(sceneName,
                                                   "jpg",
                                                   { img : String ->
                    setImageFromBase64(img)
                })
            }
        })
    }

    fun updateSceneList() {
        obsController?.getScenes({scenes ->
            sceneList.postValue(scenes)
            obsController?.getCurrentScene({scene ->
                currentScene.postValue(scene)
            })
        })
    }

    fun switchScene(name : String) {
        obsController?.switchScene(name)
    }

    fun updateRecordingStreamingStatus() {
        obsController?.getRecordingStreamingStatus({ streaming, recording ->
            isStreaming.postValue(streaming)
            isRecording.postValue(recording)
        })
    }

    override fun onConnect() {
        connectionStatus.postValue(ConnectionStatus(
           ConnectionState.CONNECTED
        ))
        updateSceneList()
        updateRecordingStreamingStatus()
        updateImage()
    }

    override fun onDisconnect() {
        println("OUTFIELD: disconnected post")
        connectionStatus.postValue(ConnectionStatus(
           ConnectionState.DISCONNECTED
        ))
    }

    override fun onError(message : String, e : Throwable) {
        connectionStatus.postValue(ConnectionStatus(
           ConnectionState.CONNECT_ERROR, message
        ))
    }

    override fun onRecordingStatusChange(on : Boolean) {
        isRecording.postValue(on)
    }

    override fun onStreamingStatusChange(on : Boolean) {
        isStreaming.postValue(on)
    }

    override fun onSwitchScenes(sceneName : String?) {
        currentScene.postValue(sceneName)
    }

    private fun setImageFromBase64(base64 : String) {
        // from: https://freakycoder.com/android-notes-44-how-to-convert-base64-string-to-bitmap-53f98d5e57af

        // trim data:image/png;base64,
        val bytes = base64.split(",")[1]
        val decoded = Base64.decode(bytes, Base64.DEFAULT)
        val bmp = BitmapFactory.decodeByteArray(decoded, 0, decoded.size)
        previewImage.postValue(bmp)
    }
}
