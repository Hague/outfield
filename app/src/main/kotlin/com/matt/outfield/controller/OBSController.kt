
package com.matt.outfield.controller

import kotlin.coroutines.CoroutineContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

import io.obswebsocket.community.client.OBSRemoteController
import io.obswebsocket.community.client.message.event.outputs.RecordStateChangedEvent
import io.obswebsocket.community.client.message.event.outputs.StreamStateChangedEvent
import io.obswebsocket.community.client.message.event.scenes.CurrentProgramSceneChangedEvent
import io.obswebsocket.community.client.message.request.record.GetRecordStatusRequest
import io.obswebsocket.community.client.message.request.record.StartRecordRequest
import io.obswebsocket.community.client.message.request.record.StopRecordRequest
import io.obswebsocket.community.client.message.request.scenes.GetCurrentProgramSceneRequest
import io.obswebsocket.community.client.message.request.scenes.GetSceneListRequest
import io.obswebsocket.community.client.message.request.scenes.SetCurrentProgramSceneRequest
import io.obswebsocket.community.client.message.request.sources.GetSourceScreenshotRequest
import io.obswebsocket.community.client.message.request.stream.GetStreamStatusRequest
import io.obswebsocket.community.client.message.request.stream.StartStreamRequest
import io.obswebsocket.community.client.message.request.stream.StopStreamRequest
import io.obswebsocket.community.client.message.response.record.GetRecordStatusResponse
import io.obswebsocket.community.client.message.response.record.StartRecordResponse
import io.obswebsocket.community.client.message.response.record.StopRecordResponse
import io.obswebsocket.community.client.message.response.scenes.GetCurrentProgramSceneResponse
import io.obswebsocket.community.client.message.response.scenes.GetSceneListResponse
import io.obswebsocket.community.client.message.response.scenes.SetCurrentProgramSceneResponse
import io.obswebsocket.community.client.message.response.sources.GetSourceScreenshotResponse
import io.obswebsocket.community.client.message.response.stream.GetStreamStatusResponse
import io.obswebsocket.community.client.message.response.stream.StartStreamResponse
import io.obswebsocket.community.client.message.response.stream.StopStreamResponse

import com.matt.outfield.util.buildWeakSet

public class OBSController(val config : OBSConfig)
        : CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job

    private val job : Job = Job()

    private var obsRemote : OBSRemoteController? = null
    private var isConnected : Boolean = false

    /**
     * @param hostname hostname with port e.g. ws://1.2.3.4:4444"
     * @param connection password or null
     */
    public class OBSConfig(val hostname : String,
                           val port : Int,
                           val password : String?)

    public interface OBSListener {
        fun onConnect() { }
        fun onDisconnect() { }
        fun onError(message : String, e : Throwable) { }
        fun onRecordingStatusChange(on : Boolean) { }
        fun onStreamingStatusChange(on : Boolean) { }
        fun onSwitchScenes(sceneName : String?) { }
    }

    val listeners : MutableSet<OBSListener> = buildWeakSet()

    public fun connect() {
        getConnection().connect()
    }

    public fun disconnect(onError : () -> Unit = { }) {
        job.cancel()
        dispatchObs(onError, { conn ->
            conn.disconnect()
        })
    }

    public fun getIsConnected() : Boolean {
        return isConnected
    }

    public fun addListener(listener : OBSListener) {
        listeners.add(listener)
    }

    public fun removeListener(listener : OBSListener) {
        listeners.remove(listener)
    }

    public fun switchScene(sceneName : String,
                           onError : () -> Unit = { }) {
        dispatchObs(onError) { conn ->
            conn.sendRequest(
                SetCurrentProgramSceneRequest.builder()
                    .sceneName(sceneName)
                    .build(),
                { _ : SetCurrentProgramSceneResponse? -> }
            )
        }
    }

    public fun getCurrentScene(callback : (String) -> Unit,
                               onError : () -> Unit = { }) {
        dispatchObs(onError) { conn ->
            conn.sendRequest(GetCurrentProgramSceneRequest.builder().build())
            { response : GetCurrentProgramSceneResponse? ->
                    response?.getCurrentProgramSceneName()?.let { name ->
                        dispatchMain { callback(name) }
                    }
            }
        }
    }

    public fun getScenes(callback : (List<String>) -> Unit,
                         onError : () -> Unit = { }) {
        dispatchObs(onError) { conn ->
            conn.sendRequest(GetSceneListRequest.builder().build()) {
                response : GetSceneListResponse? ->
                    val scenes = response?.getScenes()?.map {
                        it.sceneName
                    } ?: listOf()
                    dispatchMain { callback(scenes) }
            }
        }
    }

    /**
     * Get whether recording or streaming
     *
     * @param callback function(isStreaming, isRecording)
     * @param onError callback if error occurred
     */
    public fun getRecordingStreamingStatus(
        callback : (Boolean, Boolean) -> Unit,
        onError : () -> Unit = { }
    ) {
        dispatchObs(onError) { conn ->
            conn.sendRequest(GetStreamStatusRequest.builder().build())
            { streamResponse : GetStreamStatusResponse? ->
                dispatchObs(onError) { conn ->
                    conn.sendRequest(GetRecordStatusRequest.builder().build())
                    { recordResponse : GetRecordStatusResponse? ->
                        dispatchMain {
                            callback(
                                streamResponse?.getOutputActive() ?: false,
                                recordResponse?.getOutputActive() ?: false,
                            )
                        }
                    }
                }
            }
        }
    }

    public fun startStreaming(onError : () -> Unit = { }) {
        dispatchObs(onError) { conn ->
            conn.sendRequest(
                StartStreamRequest.builder().build(),
                { _ : StartStreamResponse? -> },
            )
        }
    }

    public fun stopStreaming(onError : () -> Unit = { }) {
        dispatchObs(onError) { conn ->
            conn.sendRequest(
                StopStreamRequest.builder().build(),
                { _ : StopStreamResponse? -> },
            )
        }
    }

    public fun startRecording(onError : () -> Unit = { }) {
        dispatchObs(onError) { conn ->
            conn.sendRequest(
                StartRecordRequest.builder().build(),
                { _ : StartRecordResponse? -> },
            )
        }
    }

    public fun stopRecording(onError : () -> Unit = { }) {
        dispatchObs(onError) { conn ->
            conn.sendRequest(
                StopRecordRequest.builder().build(),
                { _ : StopRecordResponse? -> },
            )
        }
    }

    /**
     * Get a screenshot of a source or scene (scenes are sources)
     *
     * @param sourceName name of source or scene
     * @param imgFormat png, jpg, jpeg, or any supported format
     * @param callback with image string in base64
     * @param onError callback if error
     */
    public fun takeSourceScreenshot(sourceName : String,
                                    imgFormat : String,
                                    callback : (String) -> Unit,
                                    onError : () -> Unit = { }) {
        dispatchObs(onError) { conn ->
            conn.sendRequest(
                GetSourceScreenshotRequest.builder()
                    .sourceName(sourceName)
                    .imageFormat(imgFormat)
                    .build(),
            ) { response : GetSourceScreenshotResponse? ->
                response?.let { response ->
                    dispatchMain { callback(response.getImageData()) }
                }
            }
        }
    }

    private fun getConnection() : OBSRemoteController {
        val curRemote = obsRemote
        if (curRemote != null)
            return curRemote

        val controller = OBSRemoteController.builder()
            .autoConnect(false)
            .host(config.hostname)
            .port(config.port)
            .password(config.password)
            .lifecycle()
                .onReady {
                    isConnected = true
                    notifyListeners { it.onConnect() }
                }
                .onClose {
                    isConnected = false
                    notifyListeners { it.onDisconnect() }
                }
                .onCommunicatorError { err ->
                    isConnected = false
                    notifyListeners { it.onError(err.reason, err.throwable) }
                }
                .onControllerError { err ->
                    isConnected = false
                    notifyListeners { it.onError(err.reason, err.throwable) }
                }
                .and()
            .registerEventListener(
                CurrentProgramSceneChangedEvent::class.java,
                {
                    it?.let { event ->
                        notifyListeners {
                            it.onSwitchScenes(event.getSceneName())
                        }
                    }
                }
            ).registerEventListener(
                RecordStateChangedEvent::class.java,
                {
                    it?.let { event ->
                        notifyListeners {
                            val active = event.getOutputActive()
                            it.onRecordingStatusChange(active)
                        }
                    }
                }
            ).registerEventListener(
                StreamStateChangedEvent::class.java,
                {
                    it?.let { event ->
                        notifyListeners {
                            val active = event.getOutputActive()
                            it.onStreamingStatusChange(active)
                        }
                    }
                }
            ).build()

        obsRemote = controller

        return controller
    }

    /**
     * Make an OBS call on the IO thread
     *
     * @param onError callback if an error occurred
     * @param run command to run
     */
    private fun dispatchObs(onError : () -> Unit,
                            run : (conn : OBSRemoteController) -> Unit) {
        launch {
            if (isConnected) {
                run(getConnection())
            } else {
                onError()
            }
        }
    }

    private fun dispatchMain(run : () -> Unit) {
        launch (Dispatchers.Main) { run() }
    }

    private fun notifyListeners(f : (OBSListener) -> Unit) {
        dispatchMain {
            for (listener in listeners)
                f(listener)
        }
    }

}
